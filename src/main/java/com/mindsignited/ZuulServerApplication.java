package com.mindsignited;

import com.mindsignited.keycloak.sso.EnableKeycloakOAuth2Sso;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

/**
 *
 */
@SpringBootApplication
@EnableZuulProxy
@EnableKeycloakOAuth2Sso
public class ZuulServerApplication {
    public static void main(String[] args) {
        new SpringApplicationBuilder(ZuulServerApplication.class).web(true).run(args);
    }
}
